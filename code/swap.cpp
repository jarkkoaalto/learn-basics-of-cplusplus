#include <iostream>

int main()
{
	int a = 5, b = 10, temp;
	std::cout<<"a = " << a << " , b = " <<b<<std::endl;
	temp = a;
	a = b;
	b = temp;
	
	std::cout<<"After swapping"<<std::endl;
	std::cout<<"a = "<<a<<" , b = "<<b<<std::endl;
	return 0;
}
