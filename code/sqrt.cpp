#include <iostream>
#include <cmath>

int main()
{
	double number, squareRoot;
	std::cout<<"Enter a number: "<<std::endl;
	std::cin >> number;
	/*
	sqrt() is a library function to calculate square squareRoot 
	
	*/
	squareRoot = sqrt(number);
	std::cout<<"Square root of "<<number<<" = " <<squareRoot;
	return 0; 
}
