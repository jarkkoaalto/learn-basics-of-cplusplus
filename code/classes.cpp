#include <iostream>
#include <string>

class Student {
	public:
	 int id;	// data member (also instance variable)
	 std::string name; // 
};

int main(){
	Student s1;
	s1.id = 201;
	s1.name = "James Solo";
	std::cout<<s1.id<<std::endl;
	std::cout<<s1.name<<std::endl;
	return 0;
}
